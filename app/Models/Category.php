<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Skip assigning Timestamps.
     */
    public $timestamps = false;

    /**
     * Defining Many-to-Many Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function manyPost()
    {
        return $this->belongsToMany('App\Models\Post', 'cat_posts', 'categ_id', 'post_id');
    }
}
