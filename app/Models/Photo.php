<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['file'];

    /**
     * Setting an Accessor for Photo->path.
     */
    public function getFileAttribute($photo) {
        return "/images/" . $photo;
    }

    public function getPath() {
        return asset($this->file);
    }
}
