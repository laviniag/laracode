<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'body', 'comment_id', 'user_id', 'is_active'
    ];

    /**
     * Defining BelongsTo Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function comment()
    {
        return $this->belongsTo('App\Models\Comment', 'comment_id');
    }
}
