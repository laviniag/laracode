<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'user_id', 'photo_id'
    ];

    /**
     * Assign the soft delete option.
     */
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    use Sluggable;
    use SluggableScopeHelpers;
    /**
     * Return the sluggable configuration array for this model.
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Defining BelongsTo Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function photo()
    {
        return $this->belongsTo('App\Models\Photo', 'photo_id');
    }

    /**
     * Defining Has-Many Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function hasComment()
    {
        return $this->hasMany('App\Models\Comment');
    }

    /**
     * Defining Many-to-Many Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function manyCateg()
    {
        return $this->belongsToMany('App\Models\Category', 'cat_posts', 'post_id', 'categ_id');
    }

    /**
     * Custom Methods of Post management.
     */
    public function getCateg()
    {
        $cat_list = [];
        foreach ($this->manyCateg()->get() as $categ) {
            $cat_list[] = $categ->name;
        }
        return implode(' / ', $cat_list);
    }
    public function listCateg()
    {
        $cat_list = [null,null,null];
        $ind = 0;
        foreach ($this->manyCateg()->get() as $categ) {
            $cat_list[$ind++] = $categ->id;
        }
        return $cat_list;
    }
}
