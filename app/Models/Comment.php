<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'body', 'post_id', 'user_id', 'is_active'
    ];

    /**
     * Defining BelongsTo Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function post()
    {
        return $this->belongsTo('App\Models\Post', 'post_id');
    }

    /**
     * Defining Has-Many Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function hasReply()
    {
        return $this->hasMany('App\Models\Reply');
    }
}
