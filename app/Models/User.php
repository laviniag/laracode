<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Assets\Cryptography;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'photo_id', 'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Defining Accessors and Mutators.
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Cryptography::encrypt3($password);
    }
    public function getPasswordAttribute($password)
    {
        return Cryptography::decrypt3($password);
    }

    /**
     * Defining belongsTo Relationships
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function photo()
    {
        return $this->belongsTo('App\Models\Photo');
    }

    /**
     * Defining hasMany Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manyPost()
    {
        return $this->hasMany('App\Models\Post');
    }

    /**
     * Methods for Authorizing
     */
    public function isAdmin()
    {
        return ($this->role->name == 'admin' && $this->is_active == 1);
    }
}
