<?php
namespace    App\Assets;

class Cryptography
{
    public static function encrypt3 (string $word)
    {
        $split = str_split($word);
        $crypt = '';
        
        for ($j = 0; $j < 3; $j++) {
            $crypt .= chr(mt_rand(33, 126));
        }
        $end = strlen($word);
        for ($k = 0; $k < $end; $k++) {
            $crypt .= chr(159 - ord($split[$k]));
        }
        for ($j = 0; $j < 3; $j++) {
            $crypt .= chr(mt_rand(33, 126));
        }
        
        return $crypt;
    }
    
    public static function decrypt3 (string $crypt)
    {
        $split = str_split($crypt);
        $word = '';
        
        $end = strlen($crypt) - 3;
        for ($k = 3; $k < $end; $k++) {
            $word .= chr(159 - ord($split[$k]));
        }
        
        return $word;
    }

    
}