<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostsRequest;
use App\Models\Post;
use App\Models\Photo;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminPostsControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(6);
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::pluck('name','id')->all();
        return view('admin.posts.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {
        $input = $request->all();
        $post = new Post();
        $post->title = $input['title'];
        $post->body = $input['body'];

        $user = Auth::user();
        $post->user_id = $user->id;

        $this->fetchImage($request, $post);
        $post->save();
        foreach($request->categ as $categ) {
            $post->manyCateg()->attach($categ);
        }

        return redirect('admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::findBySlug($slug);
        if ($post) {
            $comments = $post->hasComment()->whereIsActive(1)->get();
            return view('admin.posts.show', compact('post', 'comments'));
        }
        else {
            return "Post not found !";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        if ($post) {
            $categ = $post->listCateg();
            $category = Category::pluck('name','id')->all();
            return view('admin.posts.edit', compact('post', 'categ', 'category'));
        }
        else {
            return "Post not found !";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostsRequest $request, $id)
    {
        $input = $request->all();
        $post = Post::find($id);
        if ($post) {
            $post->title = $input['title'];
            $post->body = $input['body'];

            $this->fetchImage($request, $post);
            $post->save();

            $post->manyCateg()->detach();
            foreach ($request->categ as $categ) {
                $post->manyCateg()->attach($categ);
            }
            return redirect('admin/posts');
        }
        else {
            return "Post not found !";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if ($post) {
            if ($post->photo) {
                $file_path = public_path() . $post->photo->file;
                $post->photo->delete();
                if (file_exists($file_path)) {
                    unlink ($file_path);
                }
            }
            $post->delete();
            Session::flash('post_deleted', "The post has been deleted");
            return redirect('admin/posts');
        } else {
            return "Post not found !";
        }
    }

    /**
     * Fetch and upload an image if inserted.
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     */
    private function fetchImage($request, $post)
    {
        if ($file = $request->file('photo_id')) {
            $name = 'Post'. time() .'_'. $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = Photo::create(['file' => $name]);
            $post->photo_id = $photo->id;
        }
    }
}
