<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersRequest;
use App\Models\Photo;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Session;

class AdminUsersControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','id')->all();
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        $this->validate($request, [
            'password' => 'required'
        ]);
        $input = $request->all();
        if ($file = $request->file('photo_id')) {
            $name = time() .'_'. $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = Photo::create(['file' => $name]);
            $input['photo_id'] = $photo->id;
        }
        User::create($input);

//        $user = new User();
//        foreach($request->all() as $key => $value) {
//            if (strpos($key,'_') !== 0) {
//                $user->$key = $value;
//            }
//        }
//        $user->save();

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if ($user) {
            $roles = Role::pluck('name','id')->all();
            return view('admin.users.edit', compact('user','roles'));
        } else {
            return "User not found !";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersRequest $request, $id)
    {
        $input = $request->all();

        if ($file = $request->file('photo_id')) {
            $name = time() .'_'. $file->getClientOriginalName();
            $file->move('images', $name);
            $photo = Photo::create(['file' => $name]);
            $input['photo_id'] = $photo->id;
        }
        $user = User::find($id);
        if ($input['password'] == null) {
            $input['password'] = $user->password;
        }
        $user->update($input);

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user) {
            if ($user->photo) {
                unlink(public_path() . $user->photo->file);
                $user->photo->delete();
            }
            if ($user->manyPost) {
                $user->manyPost()->delete();
            }
            $user->delete();
            Session::flash('user_deleted', "The user has been deleted");
            return redirect('admin/users');
        } else {
            return "User not found !";
        }
    }
}
