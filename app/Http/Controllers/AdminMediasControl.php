<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Photo;
use App\Assets\TableSSP;

class AdminMediasControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Photo::paginate(8);
        return view('admin.media.index', compact('photos'));
    }

    /**
     * Display a listing of the resource using SSP.
     *
     * @return \Illuminate\Http\Response
     */
    public function display()
    {
        return view('admin.media.display');
    }

    /**
     * Fetch the resources using SSP.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetch(Request $request)
    {
        $server_side = new TableSSP(getenv('DB_CONNECTION'));
        $data = $server_side->table('photos')->columns("id, file, created_at")->fetch();

        echo json_encode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.media.create');
    }

    /**
     * Store a newly uploaded resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        if ($file = $request->file('file')) {
            $name = 'Img'. time() .'_'. $file->getClientOriginalName();
            $file->move('images', $name);
            Photo::create(['file' => $name]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Delete all checked resources in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function check(Request $request)
    {
        echo ">> ckeck <br>";
        dd($request->checklist);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo ">> update $id<br>";
        dd($request->checklist);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->checklist) {
            $media = $request->checklist;
        } else {
            $media = array($id);
        }
        foreach ($media as $item) {
            $photo = Photo::find($item);
            if ($photo) {
                $file_path = public_path() . $photo->file;
                $photo->delete();
                if (file_exists($file_path)) {
                    unlink ($file_path);
                }
            }
            else {
                return "Media not found !";
            }
        }
        return redirect('admin/media');
    }
}
