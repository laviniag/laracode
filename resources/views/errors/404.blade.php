@extends('layouts.admin')

@section('title', 'Page Error')

@section('content')
    <h2>Access not allowed !</h2>
@endsection

@section('footer')
@endsection
