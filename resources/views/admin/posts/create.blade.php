@extends('layouts.admin')

@section('title', 'Create Post')

@section('content')
    <h1 class="page-header">Create Post</h1>

    {!! Form::open(['method'=>'POST', 'action'=> 'AdminPostsControl@store', 'files'=>true]) !!}
        <div class="row">
            <div class="form-group col-md-8">
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('categ[0]', 'Category:') !!}
                {!! Form::select('categ[0]', [''=>'Choose Category'] + $category, null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('categ[1]', 'Category:') !!}
                {!! Form::select('categ[1]', [''=>'Add Category'] + $category, null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('categ[2]', 'Category:') !!}
                {!! Form::select('categ[2]', [''=>'Add Category'] + $category, null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('photo_id', 'Photo:') !!} <br>
                <div class="btn btn-info btn-file">
                    {!! Form::file('photo_id', null,['class'=>'form-control'])!!}
                </div>
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('body', 'Description:') !!}
                {!! Form::textarea('body', null, ['class'=>'form-control'])!!}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-4">
                {!! Form::submit('Create Post', ['class'=>'btn btn-primary']) !!}
            </div>
        </div>
    {!! Form::close() !!}

    @include('includes.form_error')
@stop

@section('scripts')
    @include('includes.tinyeditor')
@endsection