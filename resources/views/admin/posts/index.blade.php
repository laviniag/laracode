@extends('layouts.admin')

@section('title', 'Post List')

@section('content')
    <h1 class="page-header">Post List</h1>

    @if(Session::has('user_deleted'))
        <br>
        <p class="bg-warning">{{session('post_deleted')}}</p>
    @endif

    @if(sizeof($posts) > 0)
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Photo</th>
                    <th>Owner</th>
                    <th>Category</th>
                    <th>Title</th>
                    <th>Body</th>
                    <th>Comments</th>
                    <th>Created at</th>
                    <th>Update</th>
                </tr>
            </thead>
            <tbody>

            @foreach($posts as $post)
                <tr>
                    <td>{{$post->id}}</td>
                    <td><img height="50" src="{{$post->photo ? $post->photo->getPath()
                    : 'http://placehold.it/400x400' }}" alt=""></td>
                    <td>{{$post->user->name}}</td>
                    <td>{{$post->getCateg()}}</td>
                    <td><a href="{{route('posts.show', $post->slug)}}">{{$post->title}}</a></td>
                    <td><a href="{{route('posts.show', $post->slug)}}">{{str_limit($post->body, 20)}}</a></td>
                    @if(count($post->hasComment) > 0)
                        <td><a href="{{route('comments.show', $post->id)}}">View Comments</a></td>
                    @else
                        <td> - </td>
                    @endif
                    <td>{{$post->created_at->diffForhumans()}}</td>
                    <td>{{$post->updated_at->diffForhumans()}}</td>
                    <td><a href="{{route('posts.edit', $post->id)}}">Edit Post</a></td>
               </tr>
            @endforeach
            </tbody>
        </table>

        <div class="row">
            <div class="col-sm-6 col-sm-offset-5">
                {!! $posts->render() !!}
            </div>
        </div>
    @else
        <h2>No Post has been inserted</h2>
    @endif
@endsection