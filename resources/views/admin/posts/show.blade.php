@extends('layouts.admin')

@section('title', 'Show Post')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/blog-post.css') }}" />
@endsection

@section('content')
    <h1 class="page-header">{{$post->title}}</h1>

    <div class="row">
        <!-- Blog Post Content Column -->
        <div class="col-lg-8">
            <div class="row">
                <div class="col-md-6 lead">  <!-- Author -->
                    by <a href="#">{{$post->user->name}}</a>
                </div>
                <div class="col-md-6">  <!-- Date/Time -->
                    <span class="glyphicon glyphicon-time"></span> Posted on {{$post->created_at->diffForHumans()}}
                </div>
            </div>
            <hr>
            <!-- Preview Image -->
            @if($post->photo)
                <img height="200" src="{{$post->photo->getPath()}}" alt="">
                <hr>
            @endif
            <!-- Post Content -->
            <p class="lead">{!! $post->body !!}</p>
            <!-- Comments Form -->
            <div class="well">
                {!! Form::open(['method'=>'POST', 'action'=> ['AdminCommentsControl@add', $post->id]]) !!}
                    <div class="row">
                        <div class="form-group col-md-10">
                            {!! Form::label('body', 'Leave a Comment:') !!}
                            {!! Form::textarea('body', null, ['rows'=>3, 'class'=>'form-control'])!!}
                        </div>
                        <div class="form-group col-md-2">
                            <br><br>
                            {!! Form::submit('Submit', ['class'=>'btn btn-primary pull-right']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <hr>
            <!-- Comment -->
            <div class="media">
            @foreach($comments as $comment)
              <a class="pull-left" href="#">
                <img width="80" src="{{$comment->user->photo ? $comment->user->photo->getPath()
                : 'http://placehold.it/80x80'}}" alt=""></a>
              <div class="media-body">
                <h4 class="media-heading">{{$comment->user->name}}
                   <small>{{$comment->created_at->diffForHumans()}}</small>
                </h4>
                {{$comment->body}}
                  <br>
                <button class="btn btn-success pull-right" data-toggle="collapse"
                        data-target="#reply{{$comment->id}}"> Reply
                </button>
                <div class="media">
                    <br>
                  <div class="media-body">
                    <div class="well collapse" id="reply{{$comment->id}}">
                        {!! Form::open(['method'=>'POST', 'action'=> ['AdminRepliesControl@add', $comment->id]]) !!}
                        <div class="row">
                            <div class="form-group col-md-10">
                                {!! Form::label('body', 'Leave a Replay:') !!}
                                {!! Form::textarea('body', null, ['rows'=>2, 'class'=>'form-control'])!!}
                            </div>
                            <div class="form-group col-md-2">
                                <br><br>
                                {!! Form::submit('Submit', ['class'=>'btn btn-primary pull-right']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @foreach($comment->hasReply()->whereIsActive(1)->get() as $reply)
                        <a class="pull-left" href="#">
                            <img width="64" src="{{$reply->user->photo ? $reply->user->photo->getPath()
                            : 'http://placehold.it/64x64'}}" alt=""></a>
                        <div class="media-body">
                            <h4 class="media-heading">{{$reply->user->name}}
                                <small>{{$reply->created_at->diffForHumans()}}</small>
                            </h4>
                            {{$reply->body}}
                        </div>
                        <br>
                    @endforeach
                  </div>
                </div>
              </div>
              <br>
            @endforeach
            </div>
        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">
            <!-- Blog Search Well -->
            <div class="well">
                <h4>Blog Search</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                </div>
                <!-- /.input-group -->
            </div>
            <!-- Blog Categories Well -->
            <div class="well">
                <h4>Blog Categories</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            @foreach($post->manyCateg()->get() as $category)
                                <li><a href="{{route('categories.edit', $category->id)}}">{{$category->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- /.row -->
            </div>

            <!-- Side Widget Well -->
            <div class="well">
                <h4>Side Widget Well</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection