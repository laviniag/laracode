@extends('layouts.admin')

@section('title', 'Edit Post')

@section('content')
    <h1 class="page-header">Edit Post</h1>
    
    <div class="row">
        <div class="col-sm-3">
            <img src="{{$post->photo ? $post->photo->getPath() : 'http://placehold.it/400x400'}}" alt="" class="img-responsive">
        </div>

    <div class="col-sm-9">
    {!! Form::model($post, ['method'=>'PATCH', 'action'=> ['AdminPostsControl@update', $post->id], 'files'=>true]) !!}
        <div class="row">
            <div class="form-group col-md-8">
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('categ[0]', 'Category:') !!}
                {!! Form::select('categ[0]', [''=>'Choose Category'] + $category, $categ[0], ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('categ[1]', 'Category:') !!}
                {!! Form::select('categ[1]', [''=>'Add Category'] + $category, $categ[1], ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('categ[2]', 'Category:') !!}
                {!! Form::select('categ[2]', [''=>'Add Category'] + $category, $categ[2], ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('photo_id', 'Photo:') !!} <br>
                <div class="btn btn-info btn-file">
                    {!! Form::file('photo_id', null,['class'=>'form-control'])!!}
                </div>
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('body', 'Description:') !!}
                {!! Form::textarea('body', null, ['class'=>'form-control'])!!}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-4">
                {!! Form::submit('Update Post', ['class'=>'btn btn-block btn-primary']) !!}
            </div>
        </div>
    {!! Form::close() !!}

    {!! Form::open(['method'=>'DELETE', 'action'=> ['AdminPostsControl@destroy', $post->id]]) !!}
        <div class="row">
             <div class="form-group col-md-4">
                 {!! Form::submit('Delete Post', ['class'=>'btn btn-block btn-danger']) !!}
             </div>
        </div>
    {!! Form::close() !!}
    </div>
    </div>

    @include('includes.form_error')
@endsection