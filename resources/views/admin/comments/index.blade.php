@extends('layouts.admin')

@section('title', 'Comments')

@section('content')
    <h1 class="page-header">Comments
        @if(isset($post)) <small> / Title: {{$post->title}} - User: {{$post->user->name}}</small> @endif
    </h1>

    @if(sizeof($comments) > 0)
        <table class="table">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Author</th>
                    @unless(isset($post))
                        <th>Post</th>
                        <th>User</th>
                    @endunless
                    <th>Date</th>
                    <th>Comment</th>
                </tr>
            </thead>
            <tbody>
            @foreach($comments as $comment)
                <tr>
                    <td>{{$comment->id}}</td>
                    <td>{{$comment->user->name}}</td>
                    @unless(isset($post))
                        <td>{{$comment->post->title}}</td>
                        <td>{{$comment->post->user->name}}</td>
                    @endunless
                    <td>{{$comment->created_at->diffForHumans()}}</td>
                    <td>{{str_limit($comment->body,40)}}</td>
                    <td><a href="{{route('posts.show',$comment->post->id)}}">View Post</a></td>
                    @if(count($comment->hasReply) > 0)
                        <td><a href="{{route('replies.show',$comment->id)}}">View Replies</a></td>
                    @else
                        <td> - </td>
                    @endif
                    <td>
                        @if($comment->is_active == 1)
                        {!! Form::open(['method'=>'PATCH', 'action'=> ['AdminCommentsControl@update', $comment->id]]) !!}
                            <input type="hidden" name="is_active" value="0">
                            <div class="form-group">
                                {!! Form::submit('Un-approve', ['class'=>'btn btn-success']) !!}
                            </div>
                        {!! Form::close() !!}
                        @else
                        {!! Form::open(['method'=>'PATCH', 'action'=> ['AdminCommentsControl@update', $comment->id]]) !!}
                            <input type="hidden" name="is_active" value="1">
                            <div class="form-group">
                                {!! Form::submit('Approve', ['class'=>'btn btn-info']) !!}
                            </div>
                        {!! Form::close() !!}
                        @endif
                    </td>
                    <td>
                        {!! Form::open(['method'=>'DELETE', 'action'=> ['AdminCommentsControl@destroy', $comment->id]]) !!}
                        <div class="form-group">
                            {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <h2>No Comment has been inserted</h2>
    @endif
@endsection