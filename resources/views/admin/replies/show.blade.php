@extends('layouts.admin')

@section('title', 'Replies')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/blog-post.css') }}" />
@endsection

@section('content')
    <h1 class="page-header">Replies to
        <small> / Author: {{$comment->user->name}} - Comment: {{str_limit($comment->body,30)}}</small>
    </h1>

    @if(sizeof($replies) > 0)
    <table class="table">
        <thead>
        <tr>
            <th>id</th>
            <th>Author</th>
            <th>Date</th>
            <th>Body</th>
        </tr>
        </thead>

        <tbody>
        @forelse($replies as $reply)
            <tr>
                <td>{{$reply->id}}</td>
                <td>{{$reply->user->name}}</td>
                <td>{{$reply->created_at->diffForHumans()}}</td>
                <td>{{str_limit($reply->body,40)}}</td>
                <td><a href="{{route('posts.show',$comment->post->id)}}">View Post</a></td>
                <td>
                    @if($reply->is_active == 1)
                        {!! Form::open(['method'=>'PATCH', 'action'=> ['AdminRepliesControl@update', $reply->id]]) !!}
                        <input type="hidden" name="is_active" value="0">
                        <div class="form-group">
                            {!! Form::submit('Un-approve', ['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}
                    @else
                        {!! Form::open(['method'=>'PATCH', 'action'=> ['AdminRepliesControl@update', $reply->id]]) !!}
                        <input type="hidden" name="is_active" value="1">
                        <div class="form-group">
                            {!! Form::submit('Approve', ['class'=>'btn btn-info']) !!}
                        </div>
                        {!! Form::close() !!}
                    @endif
                </td>
                <td>
                    {!! Form::open(['method'=>'DELETE', 'action'=> ['AdminRepliesControl@destroy', $reply->id]]) !!}
                    <div class="form-group">
                        {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @empty
            <h2> No replies to this comment</h2>
        @endforelse

        </tbody>
    </table>
    @else
        <h2> No replies to this comment</h2>
    @endif
@endsection