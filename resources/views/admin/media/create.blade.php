@extends('layouts.admin')

@section('title', 'Create Post')
@section('styles')
    <link rel="stylesheet" href="{{ asset('components/dropzone/css/dropzone.css') }}">
@endsection

@section('content')
    <h1 class="page-header">Upload Media</h1>

    <div class="row">
        <div class="col-md-8">
        {!! Form::open(['method'=>'POST', 'action'=> 'AdminMediasControl@upload', 'class'=>'dropzone']) !!}

        {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('components/dropzone/js/dropzone.js') }}"></script>
@endsection