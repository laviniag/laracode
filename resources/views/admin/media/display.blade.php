@extends('layouts.admin')

@section('title', 'Media List')
@section('styles')
    <link rel="stylesheet" href="{{ asset('components/datatable/css/jquery.dataTables.min.css') }}">
@endsection

@section('content')
    <h1 class="page-header">SSP Media List</h1>

    <table class="table table-bordered table-striped" id="db_seed">
        <thead>
        <tr>
            <th>Id</th>
            <th>Media</th>
            <th>Name</th>
            <th>Created</th>
        </tr>
        </thead>
    </table>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('components/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#db_seed').dataTable( {
                "processing": true,
                "serverSide": true,
                "ajax": "{{route('media.fetch')}}",
                "columns": [
                    { "data": "id", "orderable": false },
                    { "data": "file" },
                    { "data": "created_at" }
                ]
            });
        });
    </script>
@endsection