@extends('layouts.admin')

@section('title', 'Media List')

@section('content')
    <h1 class="page-header">Media List</h1>

    @if(sizeof($photos) > 0)
        <form method='POST' action="{{ route('media.check') }}">
        <table class="table table-striped table-bordered">
            <tr>
                <th><button type="submit" class="btn btn-warning">Check out</button></th>
                <th>Id</th>
                <th>Media</th>
                <th>Name</th>
                <th>Created</th>
            </tr>
            </thead>
            <tbody>
            @foreach($photos as $photo)
                <tr>
                    <td><input type="checkbox" name="checklist[]" value="{{$photo->id}}"></td>
                    <td>{{$photo->id}}</td>
                    <td><img height="50" src="{{$photo->getPath()}}" alt=""></td>
                    <td>{{str_limit($photo->file,50)}}</td>
                    <td>{{$photo->created_at ? $photo->created_at : 'no date' }}</td>
                    <td>
                    {!! Form::open(['method'=>'DELETE', 'action'=> ['AdminMediasControl@destroy', $photo->id]]) !!}
                         <div class="form-group">
                             {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                         </div>
                    {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </form>

        <div class="row">
            <div class="col-sm-6 col-sm-offset-5">
                {!! $photos->render() !!}
            </div>
        </div>
    @else
        <h2>No Media has been inserted</h2>
    @endif
@endsection