@extends('layouts.admin')

@section('title', 'Create User')

@section('content')
    <h1 class="page-header">Create User</h1>

    {!! Form::open(['method'=>'POST', 'action'=> 'AdminUsersControl@store','files'=>true]) !!}
        <div class="row">
            <div class="form-group col-md-8">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::email('email', null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('role_id', 'Role:') !!}
                {!! Form::select('role_id', [''=>'Choose Options'] + $roles, null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('is_active', 'Status:') !!}
                {!! Form::select('is_active', array(1 => 'Active', 0=> 'Not Active'), 0, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('photo_id', 'Photo:') !!} <br>
                <div class="btn btn-info btn-file">
                    {!! Form::file('photo_id', null, ['class'=>'form-control'])!!}
                </div>
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('password', 'Password:') !!}
                {!! Form::password('password', ['class'=>'form-control'])!!}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-4">
                {!! Form::submit('Create User', ['class'=>'btn btn-block btn-primary']) !!}
            </div>
        </div>
    {!! Form::close() !!}

    @include('includes.form_error')

@endsection


@section('footer')
    
@stop