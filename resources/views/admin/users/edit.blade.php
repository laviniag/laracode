@extends('layouts.admin')

@section('title', 'Edit User')

@section('content')
    <h1 class="page-header">Edit User</h1>

    <div class="row">
        <div class="col-md-3">
            <img src="{{$user->photo ? $user->photo->getPath() : 'http://placehold.it/400x400'}}" alt="" class="img-responsive img-rounded">
        </div>

        <div class="col-md-9">
        {!! Form::model($user, ['method'=>'PATCH', 'action'=> ['AdminUsersControl@update', $user->id],'files'=>true]) !!}
            <div class="row">
                <div class="form-group col-md-8">
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', null, ['class'=>'form-control'])!!}
                </div>
                <div class="form-group col-md-8">
                    {!! Form::label('email', 'Email:') !!}
                    {!! Form::email('email', null, ['class'=>'form-control'])!!}
                </div>
                <div class="form-group col-md-8">
                    {!! Form::label('role_id', 'Role:') !!}
                    {!! Form::select('role_id', $roles, null, ['class'=>'form-control'])!!}
                </div>
                <div class="form-group col-md-8">
                    {!! Form::label('is_active', 'Status:') !!}
                    {!! Form::select('is_active', array(1 => 'Active', 0=> 'Not Active'), null, ['class'=>'form-control'])!!}
                </div>
                <div class="form-group col-md-8">
                    {!! Form::label('photo_id', 'Photo:') !!} <br>
                    <div class="btn btn-info btn-file">
                        {!! Form::file('photo_id', null, ['class'=>'form-control'])!!}
                    </div>
                </div>
                <div class="form-group col-md-8">
                    {!! Form::label('password', 'Password:') !!}
                    {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'No change'])!!}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="form-group col-md-4">
                    {!! Form::submit('Update User', ['class'=>'btn btn-block btn-primary']) !!}
                </div>
            </div>
        {!! Form::close() !!}

        {!! Form::open(['method'=>'DELETE', 'action'=> ['AdminUsersControl@destroy', $user->id]]) !!}
            <div class="row">
                <div class="form-group col-md-4">
                    {!! Form::submit('Delete user', ['class'=>'btn btn-block btn-danger']) !!}
                </div>
            </div>
        {!! Form::close() !!}
        </div>
    </div>

    <div class="row">
        @include('includes.form_error')
    </div>
@endsection


@section('footer')
    
@stop