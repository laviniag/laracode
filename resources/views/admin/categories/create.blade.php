@extends('layouts.admin')

@section('title', 'Add Category')

@section('content')
    <h1 class="page-header">Add Category</h1>

    {!! Form::open(['method'=>'POST', 'action'=> 'AdminCategoriesControl@store','files'=>true]) !!}
        <div class="row">
            <div class="form-group col-md-8">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name[0]', null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name[1]', null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name[2]', null, ['class'=>'form-control'])!!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name[3]', null, ['class'=>'form-control'])!!}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-4">
                {!! Form::submit('Add Category', ['class'=>'btn btn-block btn-primary']) !!}
            </div>
        </div>
    {!! Form::close() !!}

    @include('includes.form_error')

@endsection


@section('footer')
    
@stop