@extends('layouts.admin')

@section('title', 'Edit Category')

@section('content')
    <h1 class="page-header">Edit Category</h1>

    <div class="col-md-6">
        {!! Form::model($category, ['method'=>'PATCH', 'action'=> ['AdminCategoriesControl@update', $category->id]]) !!}
        <div class="row">
            <div class="form-group col-md-8">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class'=>'form-control'])!!}
            </div>
        </div>
        <div class="row">
            <br>
            <div class="form-group col-md-4">
                {!! Form::submit('Update Category', ['class'=>'btn btn-block btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}

        {!! Form::open(['method'=>'DELETE', 'action'=> ['AdminCategoriesControl@destroy', $category->id]]) !!}
        <div class="row">
            <div class="form-group col-md-4">
                {!! Form::submit('Delete Category', ['class'=>'btn btn-block btn-danger']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
