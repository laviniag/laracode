<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
{{--    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" />--}}
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/libs.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
    {{--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>--}}
    <![endif]-->

    @yield('styles')

</head>

<body>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        @include('layouts.navbar')

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-info" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="{{route('home')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                @if(Auth::check())
                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Users<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{route('users.index')}}">All Users</a></li>
                            <li><a href="{{route('users.create')}}">Create User</a></li>
                        </ul>
                    </li>
                @endif
                <li>
                    <a href="#"><i class="fa fa-wrench fa-fw"></i> Posts<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{route('posts.index')}}">All Posts</a></li>
                        <li><a href="{{route('posts.create')}}">Create Post</a></li>
                        <li><a href="{{route('comments.index')}}">All Comments</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-wrench fa-fw"></i> Categories<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{route('categories.index')}}">All Categories</a></li>
                        <li><a href="{{route('categories.create')}}">Create Category</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-wrench fa-fw"></i> Media<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{route('media.index')}}">All Media</a></li>
                        <li><a href="{{route('media.create')}}">Upload Media</a></li>
                        <li><a href="{{route('media.display')}}">Media Table</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="flot.html">Flot Charts</a></li>
                        <li><a href="morris.html">Morris.js Charts</a></li>
                    </ul>
                </li>
                <li>
                    <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                </li>
                <li>
                    <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="panels-wells.html">Panels and Wells</a></li>
                        <li><a href="buttons.html">Buttons</a></li>
                        <li><a href="notifications.html">Notifications</a></li>
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="icons.html"> Icons</a></li>
                        <li><a href="grid.html">Grid</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="#">Second Level Item</a></li>
                        <li><a href="#">Second Level Item</a></li>
                        <li>
                            <a href="#">Third Level <span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li><a href="#">Third Level Item</a></li>
                                <li><a href="#">Third Level Item</a></li>
                                <li><a href="#">Third Level Item</a></li>
                                <li><a href="#">Third Level Item</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="active">
                    <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a class="active" href="blank.html">Blank Page</a></li>
                        <li><a href="{{route('login')}}">Login Page</a></li>
                    </ul>
                </li>
            </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

    <div class="footer">
        @yield('footer')
    </div>

</div>
<!-- /#wrapper -->


<!-- Scripts -->
<script type="text/javascript" src="{{ asset('components/jquery/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('components/bootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/libs.js') }}"></script>

@yield('scripts')

</body>
</html>
