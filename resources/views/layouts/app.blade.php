<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    @yield('styles')

<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            @include('layouts.navbar')
        </nav>

        <div class="container">
            @yield('content')
        </div>

        <div class="footer">
            @yield('footer')
        </div>
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('components/jquery/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('components/bootstrap/bootstrap.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>--}}

    @yield('scripts')
</body>
</html>
