<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/post', 'AdminPostsControl@post')->name('post');

Route::group(['middleware' => 'auth'], function() {
    Route::resource('/admin/users', 'AdminUsersControl');

    Route::resource('/admin/posts', 'AdminPostsControl');

    Route::resource('/admin/comments', 'AdminCommentsControl');

    Route::resource('/admin/media', 'AdminMediasControl');
    Route::post('/admin/media/upload', 'AdminMediasControl@upload');
    Route::delete('/admin/media/check', 'AdminMediasControl@check')->name('media.check');
    Route::get('/media/display', 'AdminMediasControl@display')->name('media.display');
    Route::get('/media/fetch', 'AdminMediasControl@fetch')->name('media.fetch');

    Route::resource('/admin/comments', 'AdminCommentsControl');
    Route::post('/admin/comments/add/{post_id}', 'AdminCommentsControl@add');

    Route::resource('/admin/replies', 'AdminRepliesControl');
    Route::post('/admin/replies/add/{comment_id}', 'AdminRepliesControl@add');
});

Route::group(['middleware' => 'admin'], function() {
    Route::resource('/admin/categories', 'AdminCategoriesControl');
});

Route::get('/', function(){
    return view('admin.index');
});
Route::get('/admin', function(){
    return view('admin.index');
});